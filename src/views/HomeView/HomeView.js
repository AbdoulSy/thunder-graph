import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { graphql } from 'react-apollo';
import { VictoryPie } from 'victory';
import gql from 'graphql-tag';

export class HomeView extends React.Component {
  constructor(props) {
    super(props)
  }

  render () {
    let user = this.props.data.user
    var name = ""
    if(user) {
      name = user.name
    }
    return (
      <div className='home'>
        <h1>Hello {name}</h1>
        <VictoryPie/>
      </div>
    )
  }
}

const MyQuery = gql`query MyQuery {user(id:"1"){name}}`;


const mapStateToProps = (state, { params }) => ({

})

const mapDispatchToProps = (dispatch) => {
  const actions = {

  }

  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

const MyComponentWithData = graphql(MyQuery)(HomeView)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyComponentWithData)
